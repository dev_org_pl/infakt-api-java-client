Homepage
========
<http://dev.org.pl/>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)

Infakt APIv2
=======
<http://www.infakt.pl/infakt_api>

Project Wiki
=======
<https://bitbucket.org/dev_org_pl/infakt-api-java-client/wiki/Home>