package pl.org.dev.apiclient.infakt;
/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Integration test - You must fill src/test/resources/creditials.properties to run this test
 *
 * @author Cyprian Śniegota
 */
public class InfaktApiClientTest {

    @Test
    public void integrationTest() throws IOException, URISyntaxException {
        Properties prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("creditials.properties"));
        AuthCreditials authCreditials = new AuthCreditials(prop.getProperty("consumerKey"), prop.getProperty("consumerSecret"), null, null);
        if (StringUtils.trimToNull(authCreditials.getConsumerKey()) == null) {
            System.out.println("[SKIPPED] INTEGRATION TEST; UPDATE src/test/resources/creditials.properties FILE TO RUN THIS TEST");
            System.out.println("[SKIPPED] DO NOT COMMIT creditials.properties FILE");
            return;
        }
        InfaktApiClient infaktApiClient = new InfaktApiClient();
        Assert.assertNull(authCreditials.getAccessToken());
        Assert.assertNull(authCreditials.getSecretToken());
        try {
            authCreditials = infaktApiClient.generateCreditialsWithToken(authCreditials, prop.getProperty("username"), prop.getProperty("password"));
        } catch (InfaktApiException e) {
            e.printStackTrace();
            Assert.fail("Exception");
        }
        Assert.assertNotNull(authCreditials.getAccessToken());
        Assert.assertNotNull(authCreditials.getSecretToken());
        String settingsUserData = null;
        try {
            settingsUserData = infaktApiClient.getSettingsUserData(authCreditials);
        } catch (InfaktApiException e) {
            e.printStackTrace();
            Assert.fail("Exception");
        }
        Assert.assertNotNull(StringUtils.trimToNull(settingsUserData));
    }
}
