package pl.org.dev.apiclient.infakt;
/*
 * Licensed to HMail.pl under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  HMail.pl licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.rs.security.oauth.client.OAuthClientUtils;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @author Cyprian Śniegota
 */
public class InfaktApiClient {

    private enum ERequestMethod {
        GET, POST
    }

    private enum EInvoiceDocType {
        ORG("org"), KOPIA("KOPIA"), ORG_KOP("org_kop"), D_ORG("d_org"), D_KOP("d_kop"),
        REGULAR("regular"), DOUBLE_REGULAR("double_regular"), DUPLICATE("duplicate"),
        ANG("ang"), ANGPOL("angpol");
        private String code;

        private EInvoiceDocType(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

    private static final String SERVICE_API_PREFIX_URL = "https://infakt.pl/api/v2/";

    private String executeService(AuthCreditials authCreditials, ERequestMethod requestMethod, String serviceUrl, Properties queryProperties, String data, String servicePrefixUrl) throws InfaktApiException {
        Response response = null;
        String returnString = "{}";
        try {
            WebClient client = WebClient.create(servicePrefixUrl);
            client.path(serviceUrl);

            StringBuilder requestUrlBuilder;
            requestUrlBuilder = new StringBuilder();
            requestUrlBuilder.append(servicePrefixUrl).append(serviceUrl);
            if (queryProperties != null) {
                Enumeration<?> enumeration;
                enumeration = queryProperties.propertyNames();
                while (enumeration.hasMoreElements()) {
                    String name = (String) enumeration.nextElement();
                    String value = StringUtils.trimToNull(queryProperties.getProperty(name));
                    if (value != null) {
                        client.query(name, value);
                    }
                }
            }
            OAuthClientUtils.Consumer consumer = new OAuthClientUtils.Consumer(authCreditials.getConsumerKey(), authCreditials.getConsumerSecret());

            String authorizationHeader = OAuthClientUtils.createAuthorizationHeader(
                    consumer, new OAuthClientUtils.Token(authCreditials.getAccessToken(), authCreditials.getSecretToken()),
                    requestMethod.name(), client.getCurrentURI().toString());

            client.header("Authorization", authorizationHeader)
                    .header("Accept", "application/json").header("Content-type", "application/json");
            if (requestMethod.equals(ERequestMethod.GET)) {
                response = client.get(Response.class);
            } else if (requestMethod.equals(ERequestMethod.POST)) {
                response = client.post(data, Response.class);
            }
        } catch (Exception e) {
            throw new InfaktApiException("Error running service [" + serviceUrl + "]", e);
        }
        returnString = response != null ? response.readEntity(String.class) : "{}";
        return returnString;
    }

    private String executeServiceGet(AuthCreditials authCreditials, String serviceUrl, Properties queryProperties) throws InfaktApiException {
        return this.executeService(authCreditials, ERequestMethod.GET, serviceUrl, queryProperties, null, SERVICE_API_PREFIX_URL);
    }

    private String executeServicePost(AuthCreditials authCreditials, String serviceUrl, Properties queryProperties, String data) throws InfaktApiException {
        return this.executeService(authCreditials, ERequestMethod.POST, serviceUrl, queryProperties, data, SERVICE_API_PREFIX_URL);
    }

    public AuthCreditials generateCreditialsWithToken(AuthCreditials authCreditials, String username, String password) throws InfaktApiException {
        try {
            AuthCreditials cloned = authCreditials.clone();
            String serviceUrl = "oauth/access_token";
            ResponseDataProvider<Form> provider = new ResponseDataProvider<>();
            WebClient client = WebClient.create("https://www.infakt.pl/", Collections.singletonList(provider));
            client.path(serviceUrl);
            client.header("Accept", "application/json").header("Content-type", "application/json");
            OAuthClientUtils.Consumer consumer = new OAuthClientUtils.Consumer(authCreditials.getConsumerKey(), authCreditials.getConsumerSecret());
            client.query("x_auth_username", username).query("x_auth_password", password).query("x_auth_mode", "oauth_token");
            OAuthClientUtils.Token accessToken = OAuthClientUtils.getRequestToken(client, consumer, new URI("https://www.infakt.pl/oauth/access_token"), null);
            cloned.setAccessToken(accessToken.getToken());
            cloned.setSecretToken(accessToken.getSecret());
            return cloned;
        } catch (Exception e) {
            throw new InfaktApiException("Error obtaining token from server", e);
        }
    }


    public String getSettingsUserData(AuthCreditials authCreditials) throws InfaktApiException {
        String serviceUrl = "settings/user_data.json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String getClientsList(AuthCreditials authCreditials, Properties queryProperties) throws InfaktApiException {
        String serviceUrl = "clients/list.json";

        return this.executeServiceGet(authCreditials, serviceUrl, queryProperties);
    }

    public String getClientsShow(AuthCreditials authCreditials, String id) throws InfaktApiException {
        String serviceUrl = "clients/show/" + id + ".json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postClientsUpdate(AuthCreditials authCreditials, String id, String data) throws InfaktApiException {
        String serviceUrl = "clients/update/" + id + ".json";

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postClientsCreate(AuthCreditials authCreditials, String data) throws InfaktApiException {
        String serviceUrl = "clients/create" + ".json";

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postClientsDelete(AuthCreditials authCreditials, String id) throws InfaktApiException {
        String serviceUrl = "clients/delete/" + id + ".json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String getInvoicesList(AuthCreditials authCreditials, Properties queryProperties) throws InfaktApiException {
        String serviceUrl = "invoices/list.json";

        return this.executeServiceGet(authCreditials, serviceUrl, queryProperties);
    }

    public String getInvoicesShow(AuthCreditials authCreditials, String id) throws InfaktApiException {
        String serviceUrl = "invoices/show/" + id + ".json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesUpdate(AuthCreditials authCreditials, String id, String data) throws InfaktApiException {
        String serviceUrl = "invoices/update/" + id + ".json";

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postInvoicesCreate(AuthCreditials authCreditials, String data) throws InfaktApiException {
        String serviceUrl = "invoices/create" + ".json";

        return this.executeServicePost(authCreditials, serviceUrl, null, data);
    }

    public String postInvoicesDelete(AuthCreditials authCreditials, String id) throws InfaktApiException {
        String serviceUrl = "invoices/delete/" + id + ".json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesPaid(AuthCreditials authCreditials, String id) throws InfaktApiException {
        String serviceUrl = "invoices/paid/" + id + ".json";

        return this.executeServiceGet(authCreditials, serviceUrl, null);
    }

    public String postInvoicesPdf(AuthCreditials authCreditials, String id, EInvoiceDocType docType) throws InfaktApiException {
        String serviceUrl = "invoices/pdf/" + id + ".json";
        Properties properties = new Properties();
        properties.setProperty("doc_type", docType.getCode());
        return this.executeServiceGet(authCreditials, serviceUrl, properties);
    }

}
